import logos
import requests, json
from flask import Flask, request, render_template

# jsonify returns content with proper mime type

app = Flask(__name__, static_url_path='', static_folder='static/nba-pvp-client/dist', template_folder='static/nba-pvp-client/dist')

# static files
@app.route('/', methods=['GET'])
def home():
    return render_template('index.html')

# search players - return player name, team logo
@app.route('/api/search', methods=['POST'])
def search():
    name: str = request.form.get('name')
    search_req: Response = requests.get('https://www.balldontlie.io/api/v1/players?search={}'.format(name))
    return search_req.text

# get player by id - return player height, weight, pos, stats, team logo
@app.route('/api/get_player', methods=['POST'])
def get_player():
    player_id: str = request.form.get('player_id')
    season: str = request.form.get('season')
    player_req: Response = requests.get('https://www.balldontlie.io/api/v1/players/{}'.format(player_id))
    stats_req: Response = requests.get('https://www.balldontlie.io/api/v1/season_averages?player_ids[]={}&seasons[]={}'
    .format(player_id, season))
    try:
        player = {'info': '', 'stats': '', 'logo_file': ''}
        player['info']: dict = json.loads(player_req.text)
        player['stats']: dict = json.loads(stats_req.text)['data'][0]
        player['logo_file']: str = logos.files[player['info']['team']['abbreviation']]
        return player
    except HTTPerror as http_error:
        return f'Oops! HTTP error: {http_error}'
    except Exception as error:
        return f'Oops! General error: {error}'

# 404
@app.errorhandler(404)
def not_found(error):
    return f'Oops! {error}'

if __name__ == '__main__':
    app.run(debug=True, port=5000)